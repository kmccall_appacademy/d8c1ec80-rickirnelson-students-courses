class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name,last_name, courses=[])
    @first_name = first_name
    @last_name = last_name
    @courses = courses
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
     @courses
  end

  def enroll(new_course)
      raise if @courses.any? {|course| new_course.conflicts_with?(course)}
      @courses << new_course unless @courses.include?(new_course)
      new_course.students << self unless new_course.students.include?(self)
  end

  def course_load
    load = Hash.new(0)
    @courses.each {|c| load[c.department] += c.credits}
    load
  end
end
