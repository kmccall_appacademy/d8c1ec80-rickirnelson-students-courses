class Course
  attr_accessor :name, :department, :credits, :days, :time_block
  def initialize(name,department,credits,days=[],time_block=0)
    @name = name
    @department = department
    @credits = credits
    @students = []
    @days = days
    @time_block = time_block
  end

  def name
    @name
  end

  def department
    @department
  end

  def credits
    @credits
  end

  def students
    @students
  end

  def add_student(student)
    @students << student
    student.enroll(self)
  end

  def conflicts_with?(course)
    overlap_day = self.days & course.days
    overlap_time = self.time_block == course.time_block
    overlap_day.size > 0 && overlap_time == true ? true : false
  end
end
